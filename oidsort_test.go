package oidsort_test

import (
	"testing"

	. "gitlab.com/martinclaro/go-oidsort"
)

func TestCompareOIDs(t *testing.T) {
	cases := []struct {
		v1       string
		v2       string
		expected int
	}{
		{"1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.1", 0},
		{"1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.2", -1},
		{"1.3.6.1.2.1.25.2.3.1.6.3", "1.3.6.1.2.1.25.2.3.1.6.2", 1},
		{"1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.10", -1},
		{"1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.10", -1},
		{"1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.1.1", -1},
		{"1.3.6.1.2.1.25.2.3.1.6.1.1", "1.3.6.1.2.1.25.2.3.1.6.1", 1},
	}

	for _, tc := range cases {
		actual := CompareOIDs(tc.v1, tc.v2)
		expected := tc.expected
		if actual != expected {
			t.Fatalf(
				"%s <=> %s\nexpected: %d\nactual: %d",
				tc.v1, tc.v2,
				expected, actual)
		}
	}
}

func BenchmarkCompareOIDs(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CompareOIDs("1.3.6.1.2.1.25.2.3.1.6.1.1", "1.3.6.1.2.1.25.2.3.1.6.1.2")
	}
}
